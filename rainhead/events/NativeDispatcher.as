package rainhead.events 
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class NativeDispatcher extends EventDispatcher 
	{
		private  static var _instance:NativeDispatcher;
		
		public function NativeDispatcher(target:flash.events.IEventDispatcher=null) 
		{
			super(target);	
			_instance = this;
		}	
		
		public function dispatchNative(event:String, data:Object):void 
		{
			var e:NativeEvent = new NativeEvent(event, data);
			dispatchEvent(e);
		}
		
		static public function get instance():NativeDispatcher 
		{
			return _instance;
		}
	}
}