package rainhead.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class NativeEvent extends Event 
	{
		public static const STARLING_INIT:String = "starling_init";
		private var _data:Object;
		
		public function NativeEvent(event:String, data:Object) 
		{
			super(event, false, false);		
			this._data = data;
		}		
		
		public function get data():Object 
		{
			return _data;
		}
	}
}