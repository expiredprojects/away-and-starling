package rainhead
{
	import away3d.containers.View3D;
	import away3d.core.managers.Stage3DManager;
	import away3d.core.managers.Stage3DProxy;
	import away3d.entities.Mesh;
	import away3d.events.Stage3DEvent;
	import away3d.materials.ColorMaterial;
	import away3d.primitives.CubeGeometry;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import rainhead.actors.TextClass;
	import rainhead.core.Overlay;
	import rainhead.events.NativeDispatcher;
	import rainhead.events.NativeEvent;
	import starling.core.Starling;

	/**
	 * ...
	 * @author Mc
	 */
	[Frame(factoryClass="rainhead.Preloader")]
	public class Main extends Sprite 
	{
		private var view:View3D;
		private var cube:Mesh;
		private var stageManager:Stage3DManager;
		private var stageProxy:Stage3DProxy;
		private var overlay:Starling;
		private var textClass:TextClass;
		private var frames:int = 0;
		private var date:Date;
		private var time:Number;
		private var old:Number;
		private var nativeDispatcher:NativeDispatcher;

		public function Main():void 
		{
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			initNative();
			initProxy();
		}
		
		private function initNative():void 
		{
			nativeDispatcher = new NativeDispatcher();
		}
		
		private function initProxy():void 
		{
			stageManager = Stage3DManager.getInstance(stage);
			stageProxy = stageManager.getFreeStage3DProxy();
			stageProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, onCreate);
		}
		
		private function onCreate(e:Stage3DEvent):void 
		{
			initAway();
			initStarling();
			nativeDispatcher.addEventListener(NativeEvent.STARLING_INIT, onStarling);
		}
		
		private function initAway():void 
		{
			view = new View3D();
			view.stage3DProxy = stageProxy;
			view.shareContext = true;
			addChild(view);
			
			cube = new Mesh(new CubeGeometry(), new ColorMaterial(0x223344));
			view.scene.addChild(cube);
			
			view.camera.z = -500;
			view.camera.y = 500;
			view.camera.lookAt(new Vector3D());
		}
		
		private function initStarling():void 
		{
			overlay = new Starling(Overlay, stage, stageProxy.viewPort, stageProxy.stage3D);
			date = new Date();
			old = date.getTime();
		}
		
		private function onStarling(e:NativeEvent):void 
		{
			nativeDispatcher.removeEventListener(NativeEvent.STARLING_INIT, onStarling);
			textClass = TextClass(e.data);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(e:Event):void 
		{
			stageProxy.clear();
			update();
			overlay.nextFrame();
			view.render();
			stageProxy.present();
		}
		
		private function update():void 
		{			
			if (++frames == 60) 
			{
				date = new Date();
				time = date.getTime();
				textClass.text = "TP60: " + (time - old)/1000;
				old = time; 
				frames = 0;
			}
			
			cube.rotationY++;
		}
	}
}