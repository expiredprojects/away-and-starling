package rainhead.core 
{
	import rainhead.actors.TextClass;
	import rainhead.events.NativeDispatcher;
	import rainhead.events.NativeEvent;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class Overlay extends Sprite 
	{
		private var textClass:TextClass;
		
		public function Overlay() 
		{
			super();
			textClass = new TextClass(70, 50, "TP60: 0", 2, 1);
			addChild(textClass);
			
			NativeDispatcher.instance.dispatchNative(NativeEvent.STARLING_INIT, textClass);
		}
	}
}